<?php
include_once $prev_url.'/clases/cls_cliente_binfactory.php';
include_once $prev_url.'/fachada/fachada_cliente_binfactory.php';
class controlador_cliente_binfactory
	{
		private $fachada_cliente_binfactory;
		public function __construct()
	{
		$this->fachada_cliente_binfactory=new fachada_cliente_binfactory();
	}
	public function guardar($objeto)
	{
		return $this->fachada_cliente_binfactory->guardar($objeto);
	}
	public function actualizar($objeto)
	{
		return $this->fachada_cliente_binfactory->actualizar($objeto);
	}
	public function eliminar($id)
	{
		return $this->fachada_cliente_binfactory->eliminar($id);
	}
	public function buscar($query)
	{
		return $this->fachada_cliente_binfactory->buscar($query);
	}
	public function buscarJSON($query)
	{
		return $this->fachada_cliente_binfactory->buscarJSON($query);
	}
	public function getCliente_binfactory($id)
	{
		return $this->fachada_cliente_binfactory->getCliente_binfactory($id);
	}
	public function getCantidadcliente_binfactory($query)
	{
		return $this->fachada_cliente_binfactory->getCantidadCliente_binfactory($query);
	}
	}//fin controlador
?>