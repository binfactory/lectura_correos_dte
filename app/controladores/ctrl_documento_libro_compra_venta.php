<?php
    $prev_url="app";
include_once 'app/clases/cls_documento_libro_compra_venta.php';
include_once 'app/fachada/fachada_documento_libro_compra_venta.php';

class controlador_documento_libro_compra_venta
	{
		private $fachada_documento_libro_compra_venta;
		public function __construct()
	{
		$this->fachada_documento_libro_compra_venta=new fachada_documento_libro_compra_venta();
	}
	public function guardar($objeto)
	{
		return $this->fachada_documento_libro_compra_venta->guardar($objeto);
	}
	public function actualizar($objeto)
	{
		return $this->fachada_documento_libro_compra_venta->actualizar($objeto);
	}
	public function eliminar($id)
	{
		return $this->fachada_documento_libro_compra_venta->eliminar($id);
	}
	public function buscar($query)
	{
		return $this->fachada_documento_libro_compra_venta->buscar($query);
	}
	public function buscarJSON($query)
	{
		return $this->fachada_documento_libro_compra_venta->buscarJSON($query);
	}
	public function getDocumento_libro_compra_venta($id)
	{
		return $this->fachada_documento_libro_compra_venta->getDocumento_libro_compra_venta($id);
	}
	public function getCantidaddocumento_libro_compra_venta($query)
	{
		return $this->fachada_documento_libro_compra_venta->getCantidadDocumento_libro_compra_venta($query);
	}
	}//fin controlador
?>
