<?php

/**
 * Created by PhpStorm.
 * User: BCH
 * Date: 06-02-2017
 * Time: 16:25
 */
include_once "app/apoyo/funciones_auxiliares.php";
include_once "app/controladores/ctrl_documento_libro_compra_venta.php";
include_once "app/fachada/fachada_documento_libro_compra_venta.php";

include_once "app/controladores/ctrl_cliente_binfactory.php";
include_once "app/fachada/fachada_cliente_binfactory.php";
include_once "app/apoyo/interaccion_bd/conexion.php";

//    include_once "";
//    if (session_status() == PHP_SESSION_NONE) {
//        session_start();
//    }

/* funciones */
function contar_archivos($ruta) {
    $total_archivos = count(glob("$ruta/{*.xml}", GLOB_BRACE));
    //echo "total archivos = ".$total_archivos;
    return $total_archivos;
}

function leer_directorio($ruta) {

    $ficheros = scandir($ruta, 1);
    return $ficheros;
}

function mover_archivo($ruta, $rut, $esCopia=false) {
    print ("<br> entre a mover");
    $folder= "archivos/procesado/" . $rut;
    if (!is_dir($folder))
    {
        echo "se crea->" . $folder;
        mkdir($folder, 0777);
    }
    if(!$esCopia)
    {
        $nueva_ruta = "archivos/procesado/" . $rut .  str_replace("dtes_proveedores/" . $rut, "", $ruta);// substr($ruta, 9);
    }
    else
    {
        $nueva_ruta = "archivos/procesado/" . $rut .  str_replace("dtes_proveedores/" . $rut, "", $ruta). fechaHoraCodificada();// substr($ruta, 9);
    }
    
    $old_ruta = str_replace("//", "/", $ruta);
    escribe_debug("Vieja ruta --> " . $old_ruta);
    escribe_debug("Nueva ruta --> " . $nueva_ruta);
    return rename($old_ruta, $nueva_ruta);
}

function eliminar_archivo($ruta) {
    $nueva_ruta = str_replace("//", "/", $ruta);
    escribe_debug($nueva_ruta);
    return unlink($nueva_ruta);
}

//end funciones

//$operacion = $_GET["op"];
$ruta = "../archivos/"; //pasa usar con switch
//borrar
/*
 * Este es el mismo código que tiene el switch, solo que lo usé para hacer una muestra con esta función.
 * */
function muestra() {
    ini_set('max_execution_time', 1400);
    //print ("Entre a muestra");
    $query = "SELECT * FROM cliente_binfactory WHERE estado_vigente='vigente'";
    $ctrlClienteBinfactory = new controlador_cliente_binfactory();
    $arrClienteBinfactory = $ctrlClienteBinfactory->buscar($query);
    $clienteBinfactory = new cliente_binfactory();
    foreach ($arrClienteBinfactory as $clienteBinfactory) {
        //$folder="dtes_proveedores/" . $clienteBinfactory->fRUTEmisor;
        $ruta="dtes_proveedores/" . $clienteBinfactory->fRUTEmisor;
        $archivos_contados = contar_archivos($ruta);
        //print("<br>Archivos contados ".$archivos_contados);
 escribe_debug ("<br> archivos->" . $archivos_contados);
        if ($archivos_contados > 0) {
            //obtiene los archivos en el directorio indicado.
            $total_archivos_leidos = leer_directorio($ruta);
            $total_lee = count($total_archivos_leidos);
            //print ("<br>cantidad archivos $total_lee");
            $cont = 0;

            while (isset($total_archivos_leidos) && $cont <= $archivos_contados) {

                escribe_debug("WHILE_ARCHIVOS");
                escribe_debug ("<br>Entre while $cont");
                $dir = $ruta . "/" . $total_archivos_leidos[$cont];
                //$dir = str_replace("/","",$dir_temp);
                escribe_debug ("<br> $dir");
                $extension = explode(".", $total_archivos_leidos[$cont]);
                escribe_debug("RUTA->" . $dir);
                escribe_debug("NOMBRE ARCHIVO->" . $extension[0]);
                escribe_debug("EXTENSION->" . $extension[count($extension)-1]);
                $extension[1]=  strtolower($extension[count($extension)-1]);
                
                if (file_exists($dir) && is_file($dir) && $extension[1] == "xml") {
                     escribe_debug("EXISTE $dir");
                    escribe_debug ("<br> existe1 $dir");
                    $xml = new DOMDocument('1.0');
                    $xml->load($dir);
                    $xml->preserveWhiteSpace = TRUE;
                    $xml->encoding = "ISO-8859-1";
                    $tagName = $xml->documentElement->tagName;
                    if (true) {
                        //varaibles
                        $tipo_doc = $xml->getElementsByTagName("TipoDTE")->item(0)->nodeValue;
                        $numero_doc = $xml->getElementsByTagName("Folio")->item(0)->nodeValue;
                        $rut_contraparte = $xml->getElementsByTagName("RUTEmisor")->item(0)->nodeValue;
                        $tasa_imp = $xml->getElementsByTagName("TasaIVA")->item(0)->nodeValue;
                        $razon_social_contraparte = $xml->getElementsByTagName("RznSoc")->item(0)->nodeValue;
                        //Tipo Impuesto[1=IVA:2=LEY 18211]
                        $tipo_impuesto = 1; //$xml->getElementsByTagName("RznSoc")->item(0)->nodeValue;
                        $fecha_emision = $xml->getElementsByTagName("FchEmis")->item(0)->nodeValue;
                        //$anulado = $xml->getElementsByTagName("RznSoc")->item(0)->nodeValue;
                        //$monto_exento = $xml->getElementsByTagName("RznSoc")->item(0)->nodeValue;
                        $monto_neto = $xml->getElementsByTagName("MntNeto")->item(0)->nodeValue;
                        $monto_iva = $xml->getElementsByTagName("IVA")->item(0)->nodeValue;
                        //$iva_uso_comun = $xml->getElementsByTagName("RznSoc")->item(0)->nodeValue;
                        //$factor_iva_uso_comun = $xml->getElementsByTagName("RznSoc")->item(0)->nodeValue;
                        $temp=$xml->getElementsByTagName("ImptoReten");
                        escribeVarDump($temp);
                        $cod_otro_imp=0;
                        $tasa_otro_imp=0;
                        $monto_otro_imp=0;
                        if($temp->length>0)
                        {
                            $cod_otro_imp = $xml->getElementsByTagName("TipoImp")->item(0)->nodeValue;
                            $tasa_otro_imp = $xml->getElementsByTagName("TasaImp")->item(0)->nodeValue;
                            $monto_otro_imp = $xml->getElementsByTagName("MontoImp")->item(0)->nodeValue;
                        }
                        $monto_total = $xml->getElementsByTagName("MntTotal")->item(0)->nodeValue;
                        //$libro_compra_venta_id = $xml->getElementsByTagName("RznSoc")->item(0)->nodeValue;

                        /*
                         * Revisar que las variables comentadas, no estén en condición NOT NULL, para la inserción.
                         * */
                        $query = "SELECT * FROM documento_libro_compra_venta WHERE rut_contraparte='$rut_contraparte' AND numero_doc='$numero_doc';";
                        escribe_debug ("<br>".$query);
                        $arrayDocCompra = ejecutaQueryConRetorno($query);
                        if (!empty($arrayDocCompra)) {
                            //$dir.=fechaHoraCodificada();
                            if (mover_archivo($dir,$clienteBinfactory->fRUTEmisor, true))
                                print("El archivo " . $total_archivos_leidos[$cont] . " ya ha sido procesado.");
                        } else {
                            escribe_debug("El archivo ".$total_archivos_leidos[$cont]." se procesará.");
							echo ("El archivo ".$total_archivos_leidos[$cont]." se procesará.") ."</br>";
                            $ctrlDoc_libro_compra = new controlador_documento_libro_compra_venta();
                            $doc_libro_compra = new documento_libro_compra_venta();
                            $doc_libro_compra->setTipo_doc($tipo_doc);
                            $doc_libro_compra->setNumero_doc($numero_doc);
                            $doc_libro_compra->setRut_contraparte($rut_contraparte);
                            $doc_libro_compra->setTasa_imp($tasa_imp);
                            $doc_libro_compra->setRazon_social_contraparte($razon_social_contraparte);
                            $doc_libro_compra->setTipo_impuesto($tipo_impuesto);
                            $doc_libro_compra->setFecha_emision($fecha_emision);
                            $doc_libro_compra->setMonto_neto($monto_neto);
                            $doc_libro_compra->setMonto_iva($monto_iva);
                            $doc_libro_compra->setCod_otro_imp($cod_otro_imp);
                            $doc_libro_compra->setTasa_otro_imp($tasa_otro_imp);
                            $doc_libro_compra->setMonto_otro_imp($monto_otro_imp);
                            $doc_libro_compra->setMonto_total($monto_total);
                            $doc_libro_compra->setEstado_vigente("vigente");
                            $doc_libro_compra->setCliente_binfactory_ID($clienteBinfactory->fID);
                            $doc_libro_compra->setArchivo_XML($total_archivos_leidos[$cont]);
                            escribe_debug("muestra.archivo.ingresar");
                            escribeVarDump($doc_libro_compra);
                            $resultado = $ctrlDoc_libro_compra->guardar($doc_libro_compra);
                            escribe_debug ("<br>res->".$resultado . "<");
							echo $resultado ."</br>";
                            if (!empty($resultado)) {//si se insertó se mueve, si no se conserva para la otra vuelta.
                                mover_archivo($dir,$clienteBinfactory->fRUTEmisor);
                                //print("<br> se ha movido");
                            }
                        }
                    }
                }
//                echo "<br>";
                $cont++;
            }
//            echo "<br>Realizado.";
        } else {
            escribe_debug("No hay archivos .xml");
        }
    }
}