<?php
include_once $prev_url.'/apoyo/interaccion_bd/conexion.php';
class fachada_documento_libro_compra_venta {

	public function  __construct() {
		
	}
	public function guardar($objeto)
	{
		global $conn;
		$conn -> debug =false;
		$query = "INSERT INTO documento_libro_compra_venta (
		tipo_doc,
		numero_doc,
		rut_contraparte,
		tasa_imp,
		razon_social_contraparte,
		tipo_impuesto,
		fecha_emision,
		anulado,
		monto_exento,
		monto_neto,
		monto_iva,
		iva_uso_comun,
		factor_iva_uso_comun,
		cod_otro_imp,
		tasa_otro_imp,
		monto_otro_imp,
		monto_total,
		libro_compra_venta_ID,
		estado_vigente,
		cliente_binfactory_ID,
		mes_proceso,
		anio_proceso,
		estado_recepcion,
		fecha_revision_recepcion,
                archivo_XML,
		observacion)
		VALUES(
		\"".$objeto->getTipo_doc()."\",
		\"".$objeto->getNumero_doc()."\",
		\"".$objeto->getRut_contraparte()."\",
		\"".$objeto->getTasa_imp()."\",
		\"".$objeto->getRazon_social_contraparte()."\",
		\"".$objeto->getTipo_impuesto()."\",
		\"".$objeto->getFecha_emision()."\",
		\"".$objeto->getAnulado()."\",
		\"".$objeto->getMonto_exento()."\",
		\"".$objeto->getMonto_neto()."\",
		\"".$objeto->getMonto_iva()."\",
		\"".$objeto->getIva_uso_comun()."\",
		\"".$objeto->getFactor_iva_uso_comun()."\",
		\"".$objeto->getCod_otro_imp()."\",
		\"".$objeto->getTasa_otro_imp()."\",
		\"".$objeto->getMonto_otro_imp()."\",
		\"".$objeto->getMonto_total()."\",
		\"".$objeto->getLibro_compra_venta_ID()."\",
		\"".$objeto->getEstado_vigente()."\",
		\"".$objeto->getCliente_binfactory_ID()."\",
		\"".$objeto->getMes_proceso()."\",
		\"".$objeto->getAnio_proceso()."\",
		\"".$objeto->getEstado_recepcion()."\",
		\"".$objeto->getFecha_revision_recepcion()."\",
                \"".$objeto->getArchivo_XML()."\",
		\"".$objeto->getObservacion()."\")";
                echo $query;
		$resultado= $conn->Execute($query);
		if($resultado)
		return $conn->Insert_Id();
		else
		return false;
		
	}
	public function actualizar($objeto)
	{
		global $conn;
		$query="UPDATE documento_libro_compra_venta SET
		tipo_doc=\"".$objeto->getTipo_doc()."\",
		numero_doc=\"".$objeto->getNumero_doc()."\",
		rut_contraparte=\"".$objeto->getRut_contraparte()."\",
		tasa_imp=\"".$objeto->getTasa_imp()."\",
		razon_social_contraparte=\"".$objeto->getRazon_social_contraparte()."\",
		tipo_impuesto=\"".$objeto->getTipo_impuesto()."\",
		fecha_emision=\"".$objeto->getFecha_emision()."\",
		anulado=\"".$objeto->getAnulado()."\",
		monto_exento=\"".$objeto->getMonto_exento()."\",
		monto_neto=\"".$objeto->getMonto_neto()."\",
		monto_iva=\"".$objeto->getMonto_iva()."\",
		iva_uso_comun=\"".$objeto->getIva_uso_comun()."\",
		factor_iva_uso_comun=\"".$objeto->getFactor_iva_uso_comun()."\",
		cod_otro_imp=\"".$objeto->getCod_otro_imp()."\",
		tasa_otro_imp=\"".$objeto->getTasa_otro_imp()."\",
		monto_otro_imp=\"".$objeto->getMonto_otro_imp()."\",
		monto_total=\"".$objeto->getMonto_total()."\",
		libro_compra_venta_ID=\"".$objeto->getLibro_compra_venta_ID()."\",
		estado_vigente=\"".$objeto->getEstado_vigente()."\",
		cliente_binfactory_ID=\"".$objeto->getCliente_binfactory_ID()."\",
		mes_proceso=\"".$objeto->getMes_proceso()."\",
		anio_proceso=\"".$objeto->getAnio_proceso()."\",
		estado_recepcion=\"".$objeto->getEstado_recepcion()."\",
                archivo_XML=\"".$objeto->getArchivo_XML()."\",
		fecha_revision_recepcion=\"".$objeto->getFecha_revision_recepcion()."\",
		observacion=\"".$objeto->getObservacion()."\" WHERE ID=".$objeto->getID();
		$RecordSet = $conn -> Execute($query);
	if ($RecordSet->_numOfRows == 0) {
		return false;
		exit;
	}
	return true;
	}
	public function eliminar($id)
	{
		global $conn;
		$query="DELETE FROM documento_libro_compra_venta WHERE ID=".$id;
		$RecordSet = $conn -> Execute($query);
	if ($RecordSet->_numOfRows == 0) {
		return false;
		exit;
	}
	return true;
	}
	public function buscar($query)
	{
		global $conn;
		$conn -> debug = false;
		$RecordSet = $conn -> Execute($query);
	if ($RecordSet->_numOfRows == 0) {
		return false;
		exit;
	}
	$cont = 0;
	
	while (!$RecordSet -> EOF) {
		$objeto = new documento_libro_compra_venta($RecordSet);
		$arreglo[$cont] = $objeto;
		$cont++;
		$RecordSet -> MoveNext();
	}
	return $arreglo;
	}
	public function buscarJSON($query)
	{
		global $conn;
		$conn -> debug = false;
		$RecordSet = $conn -> Execute($query);
	if ($RecordSet->_numOfRows == 0) {
		return false;
		exit;
	}
	$cont = 0;
	
	while (!$RecordSet -> EOF) {
		$objeto = new documento_libro_compra_ventaJSON($RecordSet);
		$arreglo[$cont] = $objeto;
		$cont++;
		$RecordSet -> MoveNext();
	}
	return $arreglo;
	}
	public function getDocumento_libro_compra_venta($id)
	{
		$query="SELECT * FROM documento_libro_compra_venta WHERE ID=".$id;
		global $conn;
		$conn -> debug = false;
		$RecordSet = $conn -> Execute($query);
	if ($RecordSet->_numOfRows == 0) {
		return false;
		exit;
	}
	$documento_libro_compra_venta=new Documento_libro_compra_venta($RecordSet);
	return $documento_libro_compra_venta;
	}
	public function getCantidadDocumento_libro_compra_venta($query)
	{
		global $conn;
		$conn -> debug = false;
		$RecordSet = $conn -> Execute($query);
	if ($RecordSet->_numOfRows == 0) {
		return false;
		exit;
	}
	$cont = 0;
	while (!$RecordSet -> EOF) {
		return ($RecordSet->Fields("cantidad"));
	}
	return 0;
	
	}
	}//Fin Clase
?>