<?php

include_once $prev_url . '/apoyo/interaccion_bd/conexion.php';

class fachada_cliente_binfactory {

    public function __construct() {
        
    }

    public function guardar($objeto) {
        global $conn;
        $conn->debug = false;
        $query = "INSERT INTO cliente_binfactory (
		RutEnvia,
		FchResol,
		NroResol,
		RUTEmisor,
		RznSoc,
		GiroEmis,
		Acteco,
		DirOrigen,
		CmnaOrigen,
		certificado,
		clave,
		estado_vigente,
		empresa_autorizada_dte_ID,
		email,
		clave_email,
		host_email,
		imap_email)
		VALUES(
		\"" . $objeto->getRutEnvia() . "\",
		\"" . $objeto->getFchResol() . "\",
		\"" . $objeto->getNroResol() . "\",
		\"" . $objeto->getRUTEmisor() . "\",
		\"" . $objeto->getRznSoc() . "\",
		\"" . $objeto->getGiroEmis() . "\",
		\"" . $objeto->getActeco() . "\",
		\"" . $objeto->getDirOrigen() . "\",
		\"" . $objeto->getCmnaOrigen() . "\",
		\"" . $objeto->getCertificado() . "\",
		\"" . $objeto->getClave() . "\",
		\"" . $objeto->getEstado_vigente() . "\",
		\"" . $objeto->getEmpresa_autorizada_dte_ID() . "\",
		\"" . $objeto->getEmail() . "\",
		\"" . $objeto->getClave_email() . "\",
		\"" . $objeto->getHost_email() . "\",
		\"" . $objeto->getImap_email() . "\")";
        $resultado = $conn->Execute($query);
        if ($resultado)
            return $conn->Insert_Id();
        else
            return false;
    }

    public function actualizar($objeto) {
        global $conn;
        $query = "UPDATE cliente_binfactory SET
		RutEnvia=\"" . $objeto->getRutEnvia() . "\",
		FchResol=\"" . $objeto->getFchResol() . "\",
		NroResol=\"" . $objeto->getNroResol() . "\",
		RUTEmisor=\"" . $objeto->getRUTEmisor() . "\",
		RznSoc=\"" . $objeto->getRznSoc() . "\",
		GiroEmis=\"" . $objeto->getGiroEmis() . "\",
		Acteco=\"" . $objeto->getActeco() . "\",
		DirOrigen=\"" . $objeto->getDirOrigen() . "\",
		CmnaOrigen=\"" . $objeto->getCmnaOrigen() . "\",
		certificado=\"" . $objeto->getCertificado() . "\",
		clave=\"" . $objeto->getClave() . "\",
		estado_vigente=\"" . $objeto->getEstado_vigente() . "\",
		empresa_autorizada_dte_ID=\"" . $objeto->getEmpresa_autorizada_dte_ID() . "\",
		email=\"" . $objeto->getEmail() . "\",
		clave_email=\"" . $objeto->getClave_email() . "\",
		host_email=\"" . $objeto->getHost_email() . "\",
		imap_email=\"" . $objeto->getImap_email() . "\" WHERE ID=" . $objeto->getID();
        $RecordSet = $conn->Execute($query);
        if ($RecordSet->_numOfRows == 0) {
            return false;
            exit;
        }
        return true;
    }

    public function eliminar($id) {
        global $conn;
        $query = "DELETE FROM cliente_binfactory WHERE ID=" . $id;
        $RecordSet = $conn->Execute($query);
        if ($RecordSet->_numOfRows == 0) {
            return false;
            exit;
        }
        return true;
    }

    public function buscar($query) {
        global $conn;
        $conn->debug = false;
        $RecordSet = $conn->Execute($query);
        if ($RecordSet->_numOfRows == 0) {
            return false;
            exit;
        }
        $cont = 0;

        while (!$RecordSet->EOF) {
            $objeto = new cliente_binfactory($RecordSet);
            $arreglo[$cont] = $objeto;
            $cont++;
            $RecordSet->MoveNext();
        }
        return $arreglo;
    }

    public function buscarJSON($query) {
        global $conn;
        $conn->debug = false;
        $RecordSet = $conn->Execute($query);
        if ($RecordSet->_numOfRows == 0) {
            return false;
            exit;
        }
        $cont = 0;

        while (!$RecordSet->EOF) {
            $objeto = new cliente_binfactoryJSON($RecordSet);
            $arreglo[$cont] = $objeto;
            $cont++;
            $RecordSet->MoveNext();
        }
        return $arreglo;
    }

    public function getCliente_binfactory($id) {
        $query = "SELECT * FROM cliente_binfactory WHERE ID=" . $id;
        global $conn;
        $conn->debug = false;
        $RecordSet = $conn->Execute($query);
        if ($RecordSet->_numOfRows == 0) {
            return false;
            exit;
        }
        $cliente_binfactory = new Cliente_binfactory($RecordSet);
        return $cliente_binfactory;
    }

    public function getCantidadCliente_binfactory($query) {
        global $conn;
        $conn->debug = false;
        $RecordSet = $conn->Execute($query);
        if ($RecordSet->_numOfRows == 0) {
            return false;
            exit;
        }
        $cont = 0;
        while (!$RecordSet->EOF) {
            return ($RecordSet->Fields("cantidad"));
        }
        return 0;
    }

}

//Fin Clase
?>