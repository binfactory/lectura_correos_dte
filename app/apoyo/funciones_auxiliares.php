<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function escribe_archivo($cadena, $archivo) {
    $fp = fopen($archivo, "a+");
    fwrite($fp, $cadena . "\n");
    fclose($fp);
}
function fechaHoraCodificada()
{
    $code=strftime("%Y-%m-%d %H:%M:%S", time());
    $code=str_replace("-","",$code);
    $code=str_replace(" ","",$code);
    $code=str_replace(":","",$code);
    return $code;    
}
function getNombreMes($numero) {
    if ($numero == "1")
        return "Enero";
    else if ($numero == "2")
        return "Febrero";
    else if ($numero == "3")
        return "Marzo";
    else if ($numero == "4")
        return "Abril";
    else if ($numero == "5")
        return "Mayo";
    else if ($numero == "6")
        return "Junio";
    else if ($numero == "7")
        return "Julio";
    else if ($numero == "8")
        return "Agosto";
    else if ($numero == "9")
        return "Septiembre";
    else if ($numero == "10")
        return "Octubre";
    else if ($numero == "11")
        return "Noviembre";
    else if ($numero == "12")
        return "Diciembre";
    else
        return "";
}

function getValor($clase, $funcion) {
    if ($clase != null) {
        echo $clase->$funcion();
    } else
        echo "";
}
function getValorSoloFecha($clase, $funcion)
{
    if ($clase != null) {
        $fecha_hora=$clase->$funcion();
        $fecha_hora=trim($fecha_hora);
        
        $arrFechaHora=  explode(" ", $fecha_hora);
        echo formatoFechaEspaniol($arrFechaHora[0]);//1] . "->" . $fecha_hora;
    } else
        echo "";    
}
function seleccion($id_externo, $objeto, $metodo) {

    if ($objeto != null) {
        if ($id_externo == $objeto->$metodo())
            echo " SELECTED";
    }
    else {
    echo "";    
    }
    //if($tipo_pagina->getID()==$pagina->)
}
function seleccionReturn($id_externo, $objeto, $metodo) {

    if ($objeto != null) {
        if ($id_externo == $objeto->$metodo())
            return " SELECTED";
    }
    else {
    return "";    
    }
    //if($tipo_pagina->getID()==$pagina->)
}
function seleccionRadio($id_externo, $objeto, $metodo) {

    if ($objeto != null) {
        if ($id_externo == $objeto->$metodo())
            echo " checked=\"checked\"";
    }
    //if($tipo_pagina->getID()==$pagina->)
}


function fecha_actual() {
        date_default_timezone_set("America/Santiago");
    return date("Y-m-d", time());
}

function fechaHoraActual() {
        date_default_timezone_set("America/Santiago");
    return strftime("%Y-%m-%d %H:%M:%S", time());
    //return date("Y-m-d",time());
}

function formatoFechaEspaniol($fecha) {
    list($anio, $mes, $dia) = explode("-", $fecha);
    return $dia . "-" . $mes . "-" . $anio;
}
function formatoFechaHoraEspaniol($fecha_hora) {
    $arr=explode(' ',$fecha_hora);
    escribeVarDump($arr);
    $hora=$arr[1];
    $fecha=$arr[0];
    list($anio, $mes, $dia) = explode("-", $fecha);
    $fecha_hora=$dia . "-" . $mes . "-" . $anio . " " . $hora;
    return $fecha_hora;
}
function formatoFechaEspaniolAIngles($fecha) {
    list($dia, $mes,$anio ) = explode("-", $fecha);
    return $anio . "-" . $mes . "-" . $dia;
}

function formatoFechaHoraEstandar($fecha_hora) {
//    escribe_log(__FUNCTION__ . $fecha_hora);
    $arrTemp = explode(" ", $fecha_hora);
    $fecha = $arrTemp[0];
//    escribe_log($fecha);
    $hora = $arrTemp[1];
//    escribe_log($hora);
    list($dia, $mes, $anio) = explode("-", $fecha);
    $fecha = $anio . "-" . $mes . "-" . $dia;
    //escribe_log("fecha_final->" . $fecha . " " . $hora);
    return $fecha . " " . $hora;
}

function isMobile() {
    return eregi('ipod|iphone|android|opera mini|blackberry|palm os|windows ce', $_SERVER['HTTP_USER_AGENT']);
}

function debug($var = false, $showFrom = true) {
    if ($showFrom) {
        $calledFrom = debug_backtrace();
        $file = str_replace('\\', '/', $calledFrom[0]['file']);
        echo '' . $file . '';
        echo ' (line ' . $calledFrom[0]['line'] . ')';
    }
    echo "\n\n";
    $var = print_r($var, true);
    echo $var . "\n\n";
}

function escribe_log($valor) {
        date_default_timezone_set("America/Santiago");
    $fecha = strftime("%d-%m-%Y", time());
    $fp = fopen("log" . $fecha . ".txt", "a+");
    fwrite($fp, "\nFecha:" . strftime("%Y-%m-%d %H:%M:%S", time()) . "\n");
    fwrite($fp, "" . $valor);
    fclose($fp);
}

function escribeVarDump($var) {
    ob_start();
    var_dump($var);
    $result = ob_get_clean();
    escribe_debug($result);
}

function escribe_debug($valor) {
        date_default_timezone_set("America/Santiago");
    $fecha = strftime("%d-%m-%Y", time());
    $fp = fopen("debug" . $fecha . ".txt", "a+");
    fwrite($fp, "\nFecha:" . strftime("%Y-%m-%d %H:%M:%S", time()) . "\n");
    fwrite($fp, $valor . "\n");
    fclose($fp);

//    $fecha=strftime("%d-%m-%Y", time());
//    $fp = fopen("valores" . $fecha .".txt", "a+");
//    fwrite($fp, $datos . "\n");
//    fclose($fp);    
}

function fechaHoraActualEspaniol() {
    date_default_timezone_set("America/Santiago");
    return date('d-m-Y H:i:s', time() - 3600);
///return date("d-m-Y H:i:s",time());
}

function DivisionDoubleSinRedondear($numero, $divisor) {
    $resultado = $numero / $divisor;
    return ($resultado);
}

function ajusteTextoCelda($texto)
{
    if($texto != null && strlen($texto) > 35){
        $nuevoTexto = substr($texto, 0,35);
        
        return $nuevoTexto.'...';
    } else
        return $texto;
}
//function DivisionEntero($numero, $divisor)
//{
//    double resultado = numero / divisor;
//    return (int)Math.Ceiling(resultado);
//}
?>