<?php
class documento_libro_compra_venta
	{
		public $fID;
		public $ftipo_doc;
		public $fnumero_doc;
		public $frut_contraparte;
		public $ftasa_imp;
		public $frazon_social_contraparte;
		public $ftipo_impuesto;
		public $ffecha_emision;
		public $fanulado;
		public $fmonto_exento;
		public $fmonto_neto;
		public $fmonto_iva;
		public $fiva_uso_comun;
		public $ffactor_iva_uso_comun;
		public $fcod_otro_imp;
		public $ftasa_otro_imp;
		public $fmonto_otro_imp;
		public $fmonto_total;
		public $flibro_compra_venta_ID;
		public $festado_vigente;
		public $fcliente_binfactory_ID;
		public $fmes_proceso;
		public $fanio_proceso;
		public $festado_recepcion;
		public $ffecha_revision_recepcion;
		public $fobservacion;
                public $farchivo_XML;
                //CONSTRUCTOR
	public function __construct($RecordSet=null)
	{
		if(!is_null($RecordSet))
	{
		$this->fID=$RecordSet->Fields("ID");
		$this->ftipo_doc=$RecordSet->Fields("tipo_doc");
		$this->fnumero_doc=$RecordSet->Fields("numero_doc");
		$this->frut_contraparte=$RecordSet->Fields("rut_contraparte");
		$this->ftasa_imp=$RecordSet->Fields("tasa_imp");
		$this->frazon_social_contraparte=$RecordSet->Fields("razon_social_contraparte");
		$this->ftipo_impuesto=$RecordSet->Fields("tipo_impuesto");
		$this->ffecha_emision=$RecordSet->Fields("fecha_emision");
		$this->fanulado=$RecordSet->Fields("anulado");
		$this->fmonto_exento=$RecordSet->Fields("monto_exento");
		$this->fmonto_neto=$RecordSet->Fields("monto_neto");
		$this->fmonto_iva=$RecordSet->Fields("monto_iva");
		$this->fiva_uso_comun=$RecordSet->Fields("iva_uso_comun");
		$this->ffactor_iva_uso_comun=$RecordSet->Fields("factor_iva_uso_comun");
		$this->fcod_otro_imp=$RecordSet->Fields("cod_otro_imp");
		$this->ftasa_otro_imp=$RecordSet->Fields("tasa_otro_imp");
		$this->fmonto_otro_imp=$RecordSet->Fields("monto_otro_imp");
		$this->fmonto_total=$RecordSet->Fields("monto_total");
		$this->flibro_compra_venta_ID=$RecordSet->Fields("libro_compra_venta_ID");
		$this->festado_vigente=$RecordSet->Fields("estado_vigente");
		$this->fcliente_binfactory_ID=$RecordSet->Fields("cliente_binfactory_ID");
		$this->fmes_proceso=$RecordSet->Fields("mes_proceso");
		$this->fanio_proceso=$RecordSet->Fields("anio_proceso");
		$this->festado_recepcion=$RecordSet->Fields("estado_recepcion");
		$this->ffecha_revision_recepcion=$RecordSet->Fields("fecha_revision_recepcion");
		$this->fobservacion=$RecordSet->Fields("observacion");
                $this->farchivo_XML=$RecordSet->Fields("archivo_XML");
	}
	}
	public function setJSON($strJSON)
	{
		$this->fID=$strJSON["f0"];
		$this->ftipo_doc=$strJSON["f1"];
		$this->fnumero_doc=$strJSON["f2"];
		$this->frut_contraparte=$strJSON["f3"];
		$this->ftasa_imp=$strJSON["f4"];
		$this->frazon_social_contraparte=$strJSON["f5"];
		$this->ftipo_impuesto=$strJSON["f6"];
		$this->ffecha_emision=$strJSON["f7"];
		$this->fanulado=$strJSON["f8"];
		$this->fmonto_exento=$strJSON["f9"];
		$this->fmonto_neto=$strJSON["f10"];
		$this->fmonto_iva=$strJSON["f11"];
		$this->fiva_uso_comun=$strJSON["f12"];
		$this->ffactor_iva_uso_comun=$strJSON["f13"];
		$this->fcod_otro_imp=$strJSON["f14"];
		$this->ftasa_otro_imp=$strJSON["f15"];
		$this->fmonto_otro_imp=$strJSON["f16"];
		$this->fmonto_total=$strJSON["f17"];
		$this->flibro_compra_venta_ID=$strJSON["f18"];
		$this->festado_vigente=$strJSON["f19"];
		$this->fcliente_binfactory_ID=$strJSON["f20"];
		$this->fmes_proceso=$strJSON["f21"];
		$this->fanio_proceso=$strJSON["f22"];
		$this->festado_recepcion=$strJSON["f23"];
		$this->ffecha_revision_recepcion=$strJSON["f24"];
		$this->fobservacion=$strJSON["f25"];
		
		
	}
	//FUNCIONES GET
        public function getArchivo_XML(){
            return $this->farchivo_XML;
        }
        public function getID()
	{
		return $this->fID;
	}
	public function getTipo_doc()
	{
		return $this->ftipo_doc;
	}
	public function getNumero_doc()
	{
		return $this->fnumero_doc;
	}
	public function getRut_contraparte()
	{
		return $this->frut_contraparte;
	}
	public function getTasa_imp()
	{
		return $this->ftasa_imp;
	}
	public function getRazon_social_contraparte()
	{
		return $this->frazon_social_contraparte;
	}
	public function getTipo_impuesto()
	{
		return $this->ftipo_impuesto;
	}
	public function getFecha_emision()
	{
		return $this->ffecha_emision;
	}
	public function getAnulado()
	{
		return $this->fanulado;
	}
	public function getMonto_exento()
	{
		return $this->fmonto_exento;
	}
	public function getMonto_neto()
	{
		return $this->fmonto_neto;
	}
	public function getMonto_iva()
	{
		return $this->fmonto_iva;
	}
	public function getIva_uso_comun()
	{
		return $this->fiva_uso_comun;
	}
	public function getFactor_iva_uso_comun()
	{
		return $this->ffactor_iva_uso_comun;
	}
	public function getCod_otro_imp()
	{
		return $this->fcod_otro_imp;
	}
	public function getTasa_otro_imp()
	{
		return $this->ftasa_otro_imp;
	}
	public function getMonto_otro_imp()
	{
		return $this->fmonto_otro_imp;
	}
	public function getMonto_total()
	{
		return $this->fmonto_total;
	}
	public function getLibro_compra_venta_ID()
	{
		return $this->flibro_compra_venta_ID;
	}
	public function getEstado_vigente()
	{
		return $this->festado_vigente;
	}
	public function getCliente_binfactory_ID()
	{
		return $this->fcliente_binfactory_ID;
	}
	public function getMes_proceso()
	{
		return $this->fmes_proceso;
	}
	public function getAnio_proceso()
	{
		return $this->fanio_proceso;
	}
	public function getEstado_recepcion()
	{
		return $this->festado_recepcion;
	}
	public function getFecha_revision_recepcion()
	{
		return $this->ffecha_revision_recepcion;
	}
	public function getObservacion()
	{
		return $this->fobservacion;
	}
	//FUNCIONES SET
        public function setArchivo_XML($archivo_XML)
        {
            $this->farchivo_XML=$archivo_XML;
        }
        public function setID($ID)
	{
		$this->fID=$ID;
	}
	public function setTipo_doc($tipo_doc)
	{
		$this->ftipo_doc=$tipo_doc;
	}
	public function setNumero_doc($numero_doc)
	{
		$this->fnumero_doc=$numero_doc;
	}
	public function setRut_contraparte($rut_contraparte)
	{
		$this->frut_contraparte=$rut_contraparte;
	}
	public function setTasa_imp($tasa_imp)
	{
		$this->ftasa_imp=$tasa_imp;
	}
	public function setRazon_social_contraparte($razon_social_contraparte)
	{
		$this->frazon_social_contraparte=$razon_social_contraparte;
	}
	public function setTipo_impuesto($tipo_impuesto)
	{
		$this->ftipo_impuesto=$tipo_impuesto;
	}
	public function setFecha_emision($fecha_emision)
	{
		$this->ffecha_emision=$fecha_emision;
	}
	public function setAnulado($anulado)
	{
		$this->fanulado=$anulado;
	}
	public function setMonto_exento($monto_exento)
	{
		$this->fmonto_exento=$monto_exento;
	}
	public function setMonto_neto($monto_neto)
	{
		$this->fmonto_neto=$monto_neto;
	}
	public function setMonto_iva($monto_iva)
	{
		$this->fmonto_iva=$monto_iva;
	}
	public function setIva_uso_comun($iva_uso_comun)
	{
		$this->fiva_uso_comun=$iva_uso_comun;
	}
	public function setFactor_iva_uso_comun($factor_iva_uso_comun)
	{
		$this->ffactor_iva_uso_comun=$factor_iva_uso_comun;
	}
	public function setCod_otro_imp($cod_otro_imp)
	{
		$this->fcod_otro_imp=$cod_otro_imp;
	}
	public function setTasa_otro_imp($tasa_otro_imp)
	{
		$this->ftasa_otro_imp=$tasa_otro_imp;
	}
	public function setMonto_otro_imp($monto_otro_imp)
	{
		$this->fmonto_otro_imp=$monto_otro_imp;
	}
	public function setMonto_total($monto_total)
	{
		$this->fmonto_total=$monto_total;
	}
	public function setLibro_compra_venta_ID($libro_compra_venta_ID)
	{
		$this->flibro_compra_venta_ID=$libro_compra_venta_ID;
	}
	public function setEstado_vigente($estado_vigente)
	{
		$this->festado_vigente=$estado_vigente;
	}
	public function setCliente_binfactory_ID($cliente_binfactory_ID)
	{
		$this->fcliente_binfactory_ID=$cliente_binfactory_ID;
	}
	public function setMes_proceso($mes_proceso)
	{
		$this->fmes_proceso=$mes_proceso;
	}
	public function setAnio_proceso($anio_proceso)
	{
		$this->fanio_proceso=$anio_proceso;
	}
	public function setEstado_recepcion($estado_recepcion)
	{
		$this->festado_recepcion=$estado_recepcion;
	}
	public function setFecha_revision_recepcion($fecha_revision_recepcion)
	{
		$this->ffecha_revision_recepcion=$fecha_revision_recepcion;
	}
	public function setObservacion($observacion)
	{
		$this->fobservacion=$observacion;
	}
	
	}//FIN CLASE
?>