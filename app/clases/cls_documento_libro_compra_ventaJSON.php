<?php
class documento_libro_compra_ventaJSON
	{
	public $f0;//ID
	public $f1;//tipo_doc
	public $f2;//numero_doc
	public $f3;//rut_contraparte
	public $f4;//tasa_imp
	public $f5;//razon_social_contraparte
	public $f6;//tipo_impuesto
	public $f7;//fecha_emision
	public $f8;//anulado
	public $f9;//monto_exento
	public $f10;//monto_neto
	public $f11;//monto_iva
	public $f12;//iva_uso_comun
	public $f13;//factor_iva_uso_comun
	public $f14;//cod_otro_imp
	public $f15;//tasa_otro_imp
	public $f16;//monto_otro_imp
	public $f17;//monto_total
	public $f18;//libro_compra_venta_ID
	public $f19;//estado_vigente
	public $f20;//cliente_binfactory_ID
	public $f21;//mes_proceso
	public $f22;//anio_proceso
	public $f23;//estado_recepcion
	public $f24;//fecha_revision_recepcion
	public $f25;//observacion
	//CONSTRUCTOR
	public function __construct($RecordSet=null)
	{
		if(!is_null($RecordSet))
	{
		$this->f0=$RecordSet->Fields("ID");
		$this->f1=$RecordSet->Fields("tipo_doc");
		$this->f2=$RecordSet->Fields("numero_doc");
		$this->f3=$RecordSet->Fields("rut_contraparte");
		$this->f4=$RecordSet->Fields("tasa_imp");
		$this->f5=$RecordSet->Fields("razon_social_contraparte");
		$this->f6=$RecordSet->Fields("tipo_impuesto");
		$this->f7=$RecordSet->Fields("fecha_emision");
		$this->f8=$RecordSet->Fields("anulado");
		$this->f9=$RecordSet->Fields("monto_exento");
		$this->f10=$RecordSet->Fields("monto_neto");
		$this->f11=$RecordSet->Fields("monto_iva");
		$this->f12=$RecordSet->Fields("iva_uso_comun");
		$this->f13=$RecordSet->Fields("factor_iva_uso_comun");
		$this->f14=$RecordSet->Fields("cod_otro_imp");
		$this->f15=$RecordSet->Fields("tasa_otro_imp");
		$this->f16=$RecordSet->Fields("monto_otro_imp");
		$this->f17=$RecordSet->Fields("monto_total");
		$this->f18=$RecordSet->Fields("libro_compra_venta_ID");
		$this->f19=$RecordSet->Fields("estado_vigente");
		$this->f20=$RecordSet->Fields("cliente_binfactory_ID");
		$this->f21=$RecordSet->Fields("mes_proceso");
		$this->f22=$RecordSet->Fields("anio_proceso");
		$this->f23=$RecordSet->Fields("estado_recepcion");
		$this->f24=$RecordSet->Fields("fecha_revision_recepcion");
		$this->f25=$RecordSet->Fields("observacion");
	}
	}
	
	public function setPorObjeto($objeto)
	{
		if(!is_null($objeto))
	{
		$this->f0=$objeto->fID;
		$this->f1=$objeto->ftipo_doc;
		$this->f2=$objeto->fnumero_doc;
		$this->f3=$objeto->frut_contraparte;
		$this->f4=$objeto->ftasa_imp;
		$this->f5=$objeto->frazon_social_contraparte;
		$this->f6=$objeto->ftipo_impuesto;
		$this->f7=$objeto->ffecha_emision;
		$this->f8=$objeto->fanulado;
		$this->f9=$objeto->fmonto_exento;
		$this->f10=$objeto->fmonto_neto;
		$this->f11=$objeto->fmonto_iva;
		$this->f12=$objeto->fiva_uso_comun;
		$this->f13=$objeto->ffactor_iva_uso_comun;
		$this->f14=$objeto->fcod_otro_imp;
		$this->f15=$objeto->ftasa_otro_imp;
		$this->f16=$objeto->fmonto_otro_imp;
		$this->f17=$objeto->fmonto_total;
		$this->f18=$objeto->flibro_compra_venta_ID;
		$this->f19=$objeto->festado_vigente;
		$this->f20=$objeto->fcliente_binfactory_ID;
		$this->f21=$objeto->fmes_proceso;
		$this->f22=$objeto->fanio_proceso;
		$this->f23=$objeto->festado_recepcion;
		$this->f24=$objeto->ffecha_revision_recepcion;
		$this->f25=$objeto->fobservacion;
	}
	}
	//FUNCIONES GET
	public function getID()
	{
		return $this->f0;
	}
	public function getTipo_doc()
	{
		return $this->f1;
	}
	public function getNumero_doc()
	{
		return $this->f2;
	}
	public function getRut_contraparte()
	{
		return $this->f3;
	}
	public function getTasa_imp()
	{
		return $this->f4;
	}
	public function getRazon_social_contraparte()
	{
		return $this->f5;
	}
	public function getTipo_impuesto()
	{
		return $this->f6;
	}
	public function getFecha_emision()
	{
		return $this->f7;
	}
	public function getAnulado()
	{
		return $this->f8;
	}
	public function getMonto_exento()
	{
		return $this->f9;
	}
	public function getMonto_neto()
	{
		return $this->f10;
	}
	public function getMonto_iva()
	{
		return $this->f11;
	}
	public function getIva_uso_comun()
	{
		return $this->f12;
	}
	public function getFactor_iva_uso_comun()
	{
		return $this->f13;
	}
	public function getCod_otro_imp()
	{
		return $this->f14;
	}
	public function getTasa_otro_imp()
	{
		return $this->f15;
	}
	public function getMonto_otro_imp()
	{
		return $this->f16;
	}
	public function getMonto_total()
	{
		return $this->f17;
	}
	public function getLibro_compra_venta_ID()
	{
		return $this->f18;
	}
	public function getEstado_vigente()
	{
		return $this->f19;
	}
	public function getCliente_binfactory_ID()
	{
		return $this->f20;
	}
	public function getMes_proceso()
	{
		return $this->f21;
	}
	public function getAnio_proceso()
	{
		return $this->f22;
	}
	public function getEstado_recepcion()
	{
		return $this->f23;
	}
	public function getFecha_revision_recepcion()
	{
		return $this->f24;
	}
	public function getObservacion()
	{
		return $this->f25;
	}
	//FUNCIONES SET
	public function setID($ID)
	{
		$this->f0=$ID;
	}
	public function setTipo_doc($tipo_doc)
	{
		$this->f1=$tipo_doc;
	}
	public function setNumero_doc($numero_doc)
	{
		$this->f2=$numero_doc;
	}
	public function setRut_contraparte($rut_contraparte)
	{
		$this->f3=$rut_contraparte;
	}
	public function setTasa_imp($tasa_imp)
	{
		$this->f4=$tasa_imp;
	}
	public function setRazon_social_contraparte($razon_social_contraparte)
	{
		$this->f5=$razon_social_contraparte;
	}
	public function setTipo_impuesto($tipo_impuesto)
	{
		$this->f6=$tipo_impuesto;
	}
	public function setFecha_emision($fecha_emision)
	{
		$this->f7=$fecha_emision;
	}
	public function setAnulado($anulado)
	{
		$this->f8=$anulado;
	}
	public function setMonto_exento($monto_exento)
	{
		$this->f9=$monto_exento;
	}
	public function setMonto_neto($monto_neto)
	{
		$this->f10=$monto_neto;
	}
	public function setMonto_iva($monto_iva)
	{
		$this->f11=$monto_iva;
	}
	public function setIva_uso_comun($iva_uso_comun)
	{
		$this->f12=$iva_uso_comun;
	}
	public function setFactor_iva_uso_comun($factor_iva_uso_comun)
	{
		$this->f13=$factor_iva_uso_comun;
	}
	public function setCod_otro_imp($cod_otro_imp)
	{
		$this->f14=$cod_otro_imp;
	}
	public function setTasa_otro_imp($tasa_otro_imp)
	{
		$this->f15=$tasa_otro_imp;
	}
	public function setMonto_otro_imp($monto_otro_imp)
	{
		$this->f16=$monto_otro_imp;
	}
	public function setMonto_total($monto_total)
	{
		$this->f17=$monto_total;
	}
	public function setLibro_compra_venta_ID($libro_compra_venta_ID)
	{
		$this->f18=$libro_compra_venta_ID;
	}
	public function setEstado_vigente($estado_vigente)
	{
		$this->f19=$estado_vigente;
	}
	public function setCliente_binfactory_ID($cliente_binfactory_ID)
	{
		$this->f20=$cliente_binfactory_ID;
	}
	public function setMes_proceso($mes_proceso)
	{
		$this->f21=$mes_proceso;
	}
	public function setAnio_proceso($anio_proceso)
	{
		$this->f22=$anio_proceso;
	}
	public function setEstado_recepcion($estado_recepcion)
	{
		$this->f23=$estado_recepcion;
	}
	public function setFecha_revision_recepcion($fecha_revision_recepcion)
	{
		$this->f24=$fecha_revision_recepcion;
	}
	public function setObservacion($observacion)
	{
		$this->f25=$observacion;
	}
	
	}//FIN CLASE
?>