<?php

class cliente_binfactory {

    public $fID;
    public $fRutEnvia;
    public $fFchResol;
    public $fNroResol;
    public $fRUTEmisor;
    public $fRznSoc;
    public $fGiroEmis;
    public $fActeco;
    public $fDirOrigen;
    public $fCmnaOrigen;
    public $fcertificado;
    public $fclave;
    public $festado_vigente;
    public $fempresa_autorizada_dte_ID;
    public $femail;
    public $fclave_email;
    public $fhost_email;
    public $fimap_email;

    //CONSTRUCTOR
    public function __construct($RecordSet = null) {
        if (!is_null($RecordSet)) {
            $this->fID = $RecordSet->Fields("ID");
            $this->fRutEnvia = $RecordSet->Fields("RutEnvia");
            $this->fFchResol = $RecordSet->Fields("FchResol");
            $this->fNroResol = $RecordSet->Fields("NroResol");
            $this->fRUTEmisor = $RecordSet->Fields("RUTEmisor");
            $this->fRznSoc = $RecordSet->Fields("RznSoc");
            $this->fGiroEmis = $RecordSet->Fields("GiroEmis");
            $this->fActeco = $RecordSet->Fields("Acteco");
            $this->fDirOrigen = $RecordSet->Fields("DirOrigen");
            $this->fCmnaOrigen = $RecordSet->Fields("CmnaOrigen");
            $this->fcertificado = $RecordSet->Fields("certificado");
            $this->fclave = $RecordSet->Fields("clave");
            $this->festado_vigente = $RecordSet->Fields("estado_vigente");
            $this->fempresa_autorizada_dte_ID = $RecordSet->Fields("empresa_autorizada_dte_ID");
            $this->femail = $RecordSet->Fields("email");
            $this->fclave_email = $RecordSet->Fields("clave_email");
            $this->fhost_email = $RecordSet->Fields("host_email");
            $this->fimap_email = $RecordSet->Fields("imap_email");
        }
    }

    public function setJSON($strJSON) {
        $this->fID = $strJSON["f0"];
        $this->fRutEnvia = $strJSON["f1"];
        $this->fFchResol = $strJSON["f2"];
        $this->fNroResol = $strJSON["f3"];
        $this->fRUTEmisor = $strJSON["f4"];
        $this->fRznSoc = $strJSON["f5"];
        $this->fGiroEmis = $strJSON["f6"];
        $this->fActeco = $strJSON["f7"];
        $this->fDirOrigen = $strJSON["f8"];
        $this->fCmnaOrigen = $strJSON["f9"];
        $this->fcertificado = $strJSON["f10"];
        $this->fclave = $strJSON["f11"];
        $this->festado_vigente = $strJSON["f12"];
        $this->fempresa_autorizada_dte_ID = $strJSON["f13"];
        $this->femail = $strJSON["f14"];
        $this->fclave_email = $strJSON["f15"];
        $this->fhost_email = $strJSON["f16"];
        $this->fimap_email = $strJSON["f17"];
    }

    //FUNCIONES GET
    public function getID() {
        return $this->fID;
    }

    public function getRutEnvia() {
        return $this->fRutEnvia;
    }

    public function getFchResol() {
        return $this->fFchResol;
    }

    public function getNroResol() {
        return $this->fNroResol;
    }

    public function getRUTEmisor() {
        return $this->fRUTEmisor;
    }

    public function getRznSoc() {
        return $this->fRznSoc;
    }

    public function getGiroEmis() {
        return $this->fGiroEmis;
    }

    public function getActeco() {
        return $this->fActeco;
    }

    public function getDirOrigen() {
        return $this->fDirOrigen;
    }

    public function getCmnaOrigen() {
        return $this->fCmnaOrigen;
    }

    public function getCertificado() {
        return $this->fcertificado;
    }

    public function getClave() {
        return $this->fclave;
    }

    public function getEstado_vigente() {
        return $this->festado_vigente;
    }

    public function getEmpresa_autorizada_dte_ID() {
        return $this->fempresa_autorizada_dte_ID;
    }

    public function getEmail() {
        return $this->femail;
    }

    public function getClave_email() {
        return $this->fclave_email;
    }

    public function getHost_email() {
        return $this->fhost_email;
    }

    public function getImap_email() {
        return $this->fimap_email;
    }

    //FUNCIONES SET
    public function setID($ID) {
        $this->fID = $ID;
    }

    public function setRutEnvia($RutEnvia) {
        $this->fRutEnvia = $RutEnvia;
    }

    public function setFchResol($FchResol) {
        $this->fFchResol = $FchResol;
    }

    public function setNroResol($NroResol) {
        $this->fNroResol = $NroResol;
    }

    public function setRUTEmisor($RUTEmisor) {
        $this->fRUTEmisor = $RUTEmisor;
    }

    public function setRznSoc($RznSoc) {
        $this->fRznSoc = $RznSoc;
    }

    public function setGiroEmis($GiroEmis) {
        $this->fGiroEmis = $GiroEmis;
    }

    public function setActeco($Acteco) {
        $this->fActeco = $Acteco;
    }

    public function setDirOrigen($DirOrigen) {
        $this->fDirOrigen = $DirOrigen;
    }

    public function setCmnaOrigen($CmnaOrigen) {
        $this->fCmnaOrigen = $CmnaOrigen;
    }

    public function setCertificado($certificado) {
        $this->fcertificado = $certificado;
    }

    public function setClave($clave) {
        $this->fclave = $clave;
    }

    public function setEstado_vigente($estado_vigente) {
        $this->festado_vigente = $estado_vigente;
    }

    public function setEmpresa_autorizada_dte_ID($empresa_autorizada_dte_ID) {
        $this->fempresa_autorizada_dte_ID = $empresa_autorizada_dte_ID;
    }

    public function setEmail($email) {
        $this->femail = $email;
    }

    public function setClave_email($clave_email) {
        $this->fclave_email = $clave_email;
    }

    public function setHost_email($host_email) {
        $this->fhost_email = $host_email;
    }

    public function setImap_email($imap_email) {
        $this->fimap_email = $imap_email;
    }

}

//FIN CLASE
?>