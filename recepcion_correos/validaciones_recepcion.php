<?php
function libxml_display_error($error)
{
    $return = "<br/>\n";
    switch ($error->level) {
        case LIBXML_ERR_WARNING:
            $return .= "<b>Warning $error->code</b>: ";
            break;
        case LIBXML_ERR_ERROR:
            $return .= "<b>Error $error->code</b>: ";
            break;
        case LIBXML_ERR_FATAL:
            $return .= "<b>Fatal Error $error->code</b>: ";
            break;
    }
    $return .= trim($error->message);
    if ($error->file) {
        $return .=    " in <b>$error->file</b>";
    }
    $return .= " on line <b>$error->line</b>\n";

    return $return;
}

function libxml_display_errors() {
    $errors = libxml_get_errors();
    foreach ($errors as $error) {
        print libxml_display_error($error);
    }
    libxml_clear_errors();
}


function getRecepEnvGlosa($EstadoRecepEnv = 99){
	
    $RecepEnvGlosa = "Envio Rechazado - Otros";
    
	if($EstadoRecepEnv == "0"){
		
        $RecepEnvGlosa = "Envio Recibido Conforme";
		
    }elseif($EstadoRecepEnv == 1){
		
        $RecepEnvGlosa = "Envio Rechazado - Error de Schema";
		
    }elseif($EstadoRecepEnv == 2){
		
        $RecepEnvGlosa = "Envio Rechazado - Error de Firma";
		
    }elseif($EstadoRecepEnv == 3){
		
        $RecepEnvGlosa = "Envio Rechazado - RUT Receptor No Corresponde";
		
    }elseif($EstadoRecepEnv == 90){
		
        $RecepEnvGlosa = "Envio Rechazado - Archivo Repetido";
		
    }elseif($EstadoRecepEnv == 91){
		
        $RecepEnvGlosa = "Envio Rechazado - Archivo Ilegible";
		
    }elseif($EstadoRecepEnv == 99){
		
        $RecepEnvGlosa = "Envio Rechazado - Otros";
		
    }
    
    return $RecepEnvGlosa;   
}

function getRecepDTEGlosa($EstadoRecepDTE = 99){
	
    $RecepDTEGlosa = "DTE No Recibido - Otros";
    
	if($EstadoRecepDTE == 0){
		
        $RecepDTEGlosa = "DTE Recibido OK";
		
    }elseif($EstadoRecepDTE == 1){
		
        $RecepDTEGlosa = "DTE No Recibido - Error de Firma";
		
    }elseif($EstadoRecepDTE == 2){
		
        $RecepDTEGlosa = "DTE No Recibido - Error en RUT Emisor";
		
    }elseif($EstadoRecepDTE == 3){
		
        $RecepDTEGlosa = "DTE No Recibido - Error en RUT Receptor";
		
    }elseif($EstadoRecepDTE == 4){
		
        $RecepDTEGlosa = "DTE No Recibido - DTE Repetido";
		
	}elseif($EstadoRecepDTE == 99){
		
        $RecepDTEGlosa = "DTE No Recibido - Otros";
		
    }
    
    return $RecepDTEGlosa;   
}

function EnvioDtesEsNoValido($xml){
	$uri = "fe/recepcion_correos/schemas/ENVIODTE/EnvioDTE_v10.xsd";
	
	/*if(!file_exists($uri)){
		return false;
	}
	*/
	if (!$xml->schemaValidate($uri)) {
		print '<b>DOMDocument::schemaValidate() Generated Errors!</b>';
		libxml_display_errors();
		return true;
	}else{
		return false;
	}
}

function DteEsNoValido($xml){
	$uri = "fe/recepcion_correos/schemas/ENVIODTE/DTE_v10.xsd";
	
	/*if(!file_exists($uri)){
		return false;
	}
	*/
	if (!$xml->schemaValidate($uri)) {
		print '<b>DOMDocument::schemaValidate() Generated Errors!</b>';
		libxml_display_errors();
		return true;
	}else{
		return false;
	}
}
?>