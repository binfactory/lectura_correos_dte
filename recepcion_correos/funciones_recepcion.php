<?php

require_once('reader.php');
require_once('validaciones_recepcion.php');
require_once("fe/vendor/xmlseclibs/XmlseclibsAdapter.php");
require_once('fe/vendor/mailer/PHPMailerAutoload.php');

$prev_url = "app/";
include_once 'app/apoyo/funciones_auxiliares.php';
include_once 'app/apoyo/interaccion_bd/conexion.php';
include_once 'app/clases/cls_producto_JSON.php';
include_once 'app/clases/cls_productoStockJSON.php';
include_once 'app/clases/cls_precio_por_clienteJSON.php';
include_once 'app/clases/cls_precio_por_volumenJSON.php';
include_once 'app/clases/cls_producto_join_lista_preciosJSON.php';
include_once 'app/clases/cls_sincronizar_tablet_producto_join_lista_preciosJSON.php';
include_once 'db.inc.php';

$direccion = opendir("app/controladores/");
while ($archivo = readdir($direccion)) {
    $path_parts = pathinfo($archivo);
    $ext = $path_parts["extension"];
    if ($archivo != "." && $archivo != ".." && $ext == "php") {
        include_once ("app/controladores/$archivo");
//   echo "include_once ('app/controladores/$archivo');";
    }
}

//require_once('framework/yii.php');

function leerCorreos() {
    /* @var $receptor Emisores */
//$transaction = Yii::app()->db->beginTransaction(); 
    $flash = "";
    try {
        escribe_debug("test");
        $reader = new mail_reader('{imap.gmail.com:993/imap/ssl/novalidate-cert/norsh}INBOX', 'binfactory.dte@gmail.com', 'xqlrubyanaya');

        //echo decode_qprint($reader->message(31));

        $mbox = $reader->connection;
        //$sorted_mbox = imap_sort($mbox, SORTDATE, 1);
        //$totalrows = imap_num_msg($mbox);

        $result = $reader->returnUnseen();
        $valid = true;

        if (is_array($result)) {
            $EnvioDTE = 0;
            $cantidad_correos = 0;
            foreach ($result as $msj) {
                $cantidad_correos++;
                if ($cantidad_correos == 100)
                    break;

                echo "----- $cantidad_correos correos leidos ----- </br>";

                $structure = imap_fetchstructure($mbox, $msj);
                $headers = imap_headerinfo($mbox, $msj);
                $reply_to = $headers->reply_to[0]->mailbox . "@" . $headers->reply_to[0]->host;
                $fromaddress = imap_utf8($headers->fromaddress);

                $attachments = array();
                if (isset($structure->parts) && count($structure->parts)) {

                    for ($i = 0; $i < count($structure->parts); $i++) {

                        $attachments[$i] = array(
                            'is_attachment' => false,
                            'filename' => '',
                            'name' => '',
                            'attachment' => ''
                        );

                        if ($structure->parts[$i]->ifdparameters) {
                            foreach ($structure->parts[$i]->dparameters as $object) {
                                if (strtolower($object->attribute) == 'filename') {
                                    $attachments[$i]['is_attachment'] = true;
                                    $attachments[$i]['filename'] = $object->value;
                                }
                            }
                        }

                        if ($structure->parts[$i]->ifparameters) {
                            foreach ($structure->parts[$i]->parameters as $object) {
                                if (strtolower($object->attribute) == 'name') {
                                    $attachments[$i]['is_attachment'] = true;
                                    $attachments[$i]['name'] = $object->value;
                                }
                            }
                        }

                        if ($attachments[$i]['is_attachment']) {
                            $attachments[$i]['attachment'] = imap_fetchbody($mbox, $msj, $i + 1);
                            if ($structure->parts[$i]->encoding == 3) { // 3 = BASE64
                                $attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
                            } elseif ($structure->parts[$i]->encoding == 4) { // 4 = QUOTED-PRINTABLE
                                $attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
                            }
                        }
                    }
                }
//var_dump($attachments);
                foreach ($attachments as $adjunto) {
                    //  var_dump($adjunto);
                    echo "uno 1<br/>";
                    var_dump($adjunto['is_attachment']);
                    if ($adjunto['is_attachment'] == 1) {
                        try {
                            $isXML = true;
                            echo "tiene archivo <br/>";
                            libxml_use_internal_errors(true);
                            $xml = new DOMDocument('1.0');
                            $xml->preserveWhiteSpace = TRUE;

                            if (!$xml->loadXML($adjunto['attachment'])) {
                                $errors = libxml_get_errors();
                                $isXML = $isXML && false;
                                echo "dos <br/>";
                            } else {
                                $isXML = $isXML && true;
                                $xml->loadXML($adjunto['attachment']);
                                $xml->encoding = "ISO-8859-1";
                                $tagName = $xml->documentElement->tagName;
                                echo "tres <br/>";
                            }
                        } catch (Exception $e) {
                            echo $e->getMessage();
                            exit();
                        }

                        if ($isXML) {
                            echo "es XML </br>";
                            switch ($tagName) {
                                case 'EnvioDTE':
                                    $EnvioDTE++;
                                    $pRutEmpresa = "14110944-8";
                                    $EsValido = true;

//                var_dump($xml);
                                    if (EnvioDtesEsNoValido($xml)) {
                                        echo "marca 1 </br>";
                                        $EnvioDTE--;
                                        $EsValido = false;
                                        $EstadoRecepEnv = 1;
                                        $RecepEnvGlosa = getRecepEnvGlosa($EstadoRecepEnv);
                                        echo "marca 2 </br>";
                                    } else {
                                        $EstadoRecepEnv = 0;
                                        $RecepEnvGlosa = getRecepEnvGlosa($EstadoRecepEnv);
                                        $SetDTE = $xml->getElementsByTagName('SetDTE')->item(0);
                                        echo "marca 3 </br>";
                                        $countDigestValue = $xml->getElementsByTagName("DigestValue")->length;
                                        $digestCount = $countDigestValue - 1;
                                        $SetDTEId = $SetDTE->getAttribute('ID');
                                        echo "marca 4 </br>";
                                        $doc = $xml->getElementsByTagName("DTE");
                                        $i = 0;
                                        foreach ($doc as $dte) {
                                            $i++;
                                        }
                                        echo "marca 5 </br>";
                                        $recep_env = new recepcion_envio();
                                        $recep_env->freceptor_ID = "1"; // Yii::app()->session['idemisor'];
                                        $recep_env->fNmbEnvio = $adjunto['filename'];
                                        $recep_env->fFchRecep = date('Y-m-d H:i:s');
                                        $recep_env->fEnvioDTEID = $SetDTEId;
                                        $recep_env->fRutEmisor = $xml->getElementsByTagName("RutEmisor")->item(0)->nodeValue;
                                        $recep_env->fRutEnvia = $xml->getElementsByTagName("RutEnvia")->item(0)->nodeValue;
                                        $recep_env->fRutReceptor = $xml->getElementsByTagName("RutReceptor")->item(0)->nodeValue;
                                        $recep_env->fEstadoRecepEnv = $EstadoRecepEnv;
                                        $recep_env->fRecepEnvGlosa = $RecepEnvGlosa;
                                        $recep_env->fTmstFirmaEnv = $xml->getElementsByTagName("TmstFirmaEnv")->item(0)->nodeValue;
                                        $recep_env->fDigest = $xml->getElementsByTagName("DigestValue")->item($digestCount)->nodeValue;
                                        $recep_env->fcorreorespuesta = $reply_to;
                                        $recep_env->fNroDTE = $i;


                                        echo "marca 6 </br>";
//                    $recepcionENVIO = new RecepciConEnvio;
//                    $boleanRE = $recepcionENVIO->model()->exists("Digest = '$recep_env->fDigest'");
                                        $query = "SELECT ID FROM recepcion_envio WHERE Digest LIKE '" . $recep_env->fDigest . "'";
                                        echo "marca 7</br>";
                                        echo $query . "</br>";

                                        $booleanDTE = ejecutaQueryExisteDato($query);
                                        if ($booleanDTE) {
                                            $EstadoRecepEnv = 90;
                                            $RecepEnvGlosa = getRecepEnvGlosa($EstadoRecepEnv);
                                        }
                                        //Este sería Binfactory                                
                                        // $receptor = Emisores::model()->findByPK(Yii::app()->session['idemisor']);
//                    if($receptor !== NULL){
//                        if($receptor->rut != $recep_env->RutReceptor){
//                            throw new Exception("El Rut receptor no corresponder a la empresa.");
//                        }
//                    }
                                        $ctrlRecepcionEnvio = new controlador_recepcion_envio();
//                    if($recep_env->save()){ 
                                        $recep_env->fID = $ctrlRecepcionEnvio->guardar($recep_env);
                                        $recep_env->fCodEnvio = $recep_env->fID;
                                        $ctrlRecepcionEnvio->actualizar($recep_env);
                                        // $pRutEmpresa   = substr($recep_env->receptorFK->rut,0, -2);
                                        $xml->save("empresas/$pRutEmpresa/recepciones/bf_RE_" . $adjunto['filename']);
                                        echo "marca 8</br>";
                                        /* AQUÍ SE CREA LA ASOCIACION ENTRE LA RESPUESTA Y EL ENVIO */
                                        $respuesta_dte = new respuestas_dte();
                                        $respuesta_dte->frecepcionEnvio_ID = $recep_env->fID;
                                        $respuesta_dte->freceptor_ID = "1"; // $receptor->fidemisor;
                                        $respuesta_dte->ftipo_respuesta = "RECEPCION CORRECTA"; // Constantes::RESPUESTA_RECEPCION;
                                        $ctrlRespuestaDTE = new controlador_respuestas_dte();
                                        $respuesta_dte->fID = $ctrlRespuestaDTE->guardar($respuesta_dte);
//                            if($respuesta_dte->save()){
//                               $valid = $valid && true;
//                            }else{
//                               unlink("empresas/$pRutEmpresa/recepciones/FACTURAFACIL_RE_".$adjunto['filename']);
//                            }
                                        /* AQUÍ SE CREA LA ASOCIACION ENTRE LA RESPUESTA Y EL ENVIO */
//                    }else{
//                                                        
//                        foreach ($recep_env->getErrors() as $attribute => $error){
//                            foreach ($error as $message){
//                                $flash .= ($attribute.": ".$message . "<br/>");
//                            }
//                        } 
//                        Yii::app()->user->setFlash('error', $flash);
//                        $valid = $valid && false;
//                        throw new Exception('No se logro terminar de guardar el envio');
//                    }
//                    if(!$valid){
//                        $transaction->rollback();
//                        throw new Exception('No se logro terminar de guardar el envio');
//                    }
                                        echo "marca 9</br>";
                                        $timezone = new DateTimeZone('America/Santiago');
                                        $date = new DateTime('', $timezone);
                                        $fechaTimbre = $date->format('Y-m-d\TH:i:s');

                                        $caratula = new caratula_respuesta();
                                        $caratula->frespuesta_DTE_ID = $respuesta_dte->fID;
                                        $caratula->freceptor_ID = "1"; //$receptor->idemisor;
                                        $caratula->ftipo_respuesta = "RECEPCION CORRECTA"; //Constantes::RESPUESTA_RECEPCION;
                                        $caratula->fRutResponde = "76169654-8"; //$receptor->rut;
                                        $caratula->fRutRecibe = $recep_env->fRutEmisor;
                                        $caratula->fNroDetalles = 1;
                                        $caratula->fTmstFirmaResp = $fechaTimbre;
                                        //$caratula->save();
                                        $ctrlCaratulaRespuesta = new controlador_caratula_respuesta();
                                        $caratula->fID = $ctrlCaratulaRespuesta->guardar($caratula);
                                        echo "marca 10</br>";
                                        $XMLRESPUESTA = new XMLWriter();
//                    touch("empresas/$pRutEmpresa/respuestas/FF_RSPENV_".$recep_env->fID.".xml");
//                    $uri = realpath("empresas/$pRutEmpresa/respuestas/FF_RSPENV_".$recep_env->frecepcion_envio_ID.".xml");                                    
                                        touch("empresas/$pRutEmpresa/respuestas/FF_RSPENV_" . $recep_env->fID . ".xml");
                                        $uri = realpath("empresas/$pRutEmpresa/respuestas/FF_RSPENV_" . $recep_env->fID . ".xml");

                                        echo "uri <br/>";
                                        echo $uri;
                                        echo "uri2 <br/>";
                                        $XMLRESPUESTA->openURI($uri);
                                        $XMLRESPUESTA->setIndent(true);
                                        $XMLRESPUESTA->startDocument('1.0', 'ISO-8859-1');
                                        $XMLRESPUESTA->startElement('RespuestaDTE');
                                        $XMLRESPUESTA->writeAttribute('version', '1.0');
                                        $XMLRESPUESTA->writeAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
                                        $XMLRESPUESTA->writeAttribute('xmlns', 'http://www.sii.cl/SiiDte');
                                        $XMLRESPUESTA->writeAttribute('xsi:schemaLocation', 'http://www.sii.cl/SiiDte RespuestaEnvioDTE_v10.xsd');
                                        echo "marca 11</br>";
                                        $XMLRESPUESTA->startElement('Resultado');
                                        $XMLRESPUESTA->writeAttribute('ID', 'Respuesta#' . "$caratula->frespuesta_DTE_ID");
                                        $XMLRESPUESTA->startElement('Caratula');
                                        $XMLRESPUESTA->writeAttribute('version', '1.0');
                                        $XMLRESPUESTA->writeElement('RutResponde', $caratula->fRutResponde);
                                        $XMLRESPUESTA->writeElement('RutRecibe', $caratula->fRutRecibe);
                                        $XMLRESPUESTA->writeElement('IdRespuesta', "$caratula->frespuesta_DTE_ID");
                                        $XMLRESPUESTA->writeElement('NroDetalles', "$caratula->fNroDetalles");
                                        $XMLRESPUESTA->writeElement('TmstFirmaResp', "$caratula->fTmstFirmaResp");
                                        $XMLRESPUESTA->endElement();
                                        echo "marca 12</br>";
                                        $XMLRESPUESTA->startElement('RecepcionEnvio');
                                        $XMLRESPUESTA->writeElement('NmbEnvio', $adjunto['filename']);
                                        $XMLRESPUESTA->writeElement('FchRecep', $fechaTimbre);
                                        $XMLRESPUESTA->writeElement('CodEnvio', "$recep_env->fCodEnvio");
                                        $XMLRESPUESTA->writeElement('EnvioDTEID', $SetDTEId);
                                        $XMLRESPUESTA->writeElement('EstadoRecepEnv', "$recep_env->fEstadoRecepEnv");
                                        $XMLRESPUESTA->writeElement('RecepEnvGlosa', $recep_env->fRecepEnvGlosa);
                                        $XMLRESPUESTA->writeElement('NroDTE', $recep_env->fNroDTE);
                                        echo "marca 13</br>";
                                        foreach ($doc as $dte) {
                                            $dx = $xml->saveXML($dte);
                                            $dte_xml = new DOMDocument('1.0');
                                            $dte_xml->preserveWhiteSpace = TRUE;
                                            $dte_xml->loadXML($dx);
                                            $dte_xml->encoding = "ISO-8859-1";
                                            echo "marca 14</br>";
                                            $NodoDocumento = $dte_xml->getElementsByTagName('Documento')->item(0);
                                            $DocumentoID = $NodoDocumento->getAttribute('ID');

                                            $recep_dte = new recepcion_dte();
                                            $recep_dte->frespuestas_DTE_ID = $respuesta_dte->fID;
                                            $recep_dte->freceptor_ID = "1"; //<-BF $receptor->fID;
                                            $recep_dte->fTipoDTE = $dte_xml->getElementsByTagName("TipoDTE")->item(0)->nodeValue;
                                            $recep_dte->fFolio = $dte_xml->getElementsByTagName("Folio")->item(0)->nodeValue;
                                            $recep_dte->fFchEmis = $dte_xml->getElementsByTagName("FchEmis")->item(0)->nodeValue;
                                            $recep_dte->fRutEmisor = $dte_xml->getElementsByTagName("RUTEmisor")->item(0)->nodeValue;
                                            $recep_dte->fRutRecep = $dte_xml->getElementsByTagName("RUTRecep")->item(0)->nodeValue;
                                            $recep_dte->fMntTotal = $dte_xml->getElementsByTagName("MntTotal")->item(0)->nodeValue;
                                            $recep_dte->fEstadoRecepDTE = 0;
                                            $recep_dte->fRecepDTEGlosa = getRecepDTEGlosa($recep_dte->EstadoRecepDTE);
                                            echo "marca 15</br>";
//                                $existeRecepcionDTE = new RecepcionDte;
                                            $recep_dte->fDigestValue = $dte_xml->getElementsByTagName("DigestValue")->item(0)->nodeValue;
//                                $booleanDTE = $existeRecepcionDTE->model()->exists("DigestValue = '$recep_dte->fDigestValue'");

                                            $query = "SELECT ID FROM recepcion_DTE WHERE DigestValue LIKE '" . $recep_dte->fDigestValue . "'";
                                            $booleanDTE = ejecutaQueryExisteDato($query);
                                            if ($booleanDTE) {
                                                $recep_dte->fEstadoRecepDTE = 4;
                                                $recep_dte->fRecepDTEGlosa = getRecepDTEGlosa($recep_dte->fEstadoRecepDTE);
                                            }


                                            $recep_dte->fnombre_archivo = "DTE_E" . $recep_dte->fRutEmisor . "_" . $DocumentoID . "_F" . $recep_dte->fFolio . ".xml";


                                            if (validaRut($recep_dte->fRutEmisor) == false) {
                                                $recep_dte->fEstadoRecepDTE = 2;
                                                $recep_dte->fRecepDTEGlosa = getRecepDTEGlosa($recep_dte->fEstadoRecepDTE);
                                            }

                                            if (validaRut($recep_dte->fRutRecep) == false) {
                                                $recep_dte->fEstadoRecepDTE = 3;
                                                $recep_dte->fRecepDTEGlosa = getRecepDTEGlosa($recep_dte->fEstadoRecepDTE);
                                            }

                                            if ($receptor !== NULL) {
                                                if ($receptor->rut != $recep_dte->RutRecep) {
                                                    $recep_dte->fEstadoRecepDTE = 3;
                                                    $recep_dte->fRecepDTEGlosa = getRecepDTEGlosa($recep_dte->fEstadoRecepDTE);
                                                }
                                            }
                                            echo "marca 16</br>";
                                            $ctrlRecepcionDTE = new controlador_recepcion_dte();
                                            $recep_dte->fID = $ctrlRecepcionDTE->guardar($recep_dte);

//                            if($recep_dte->save()){
//                                $valid = $valid && true;
//                            }else{
//                                $valid = $valid && false;
//                            }

                                            $XMLRESPUESTA->startElement('RecepcionDTE');
                                            $XMLRESPUESTA->writeElement('TipoDTE', "$recep_dte->fTipoDTE");
                                            $XMLRESPUESTA->writeElement('Folio', "$recep_dte->fFolio");
                                            $XMLRESPUESTA->writeElement('FchEmis', $recep_dte->fFchEmis);
                                            $XMLRESPUESTA->writeElement('RUTEmisor', $recep_dte->fRutEmisor);
                                            $XMLRESPUESTA->writeElement('RUTRecep', $recep_dte->fRutRecep);
                                            $XMLRESPUESTA->writeElement('MntTotal', "$recep_dte->fMntTotal");
                                            $XMLRESPUESTA->writeElement('EstadoRecepDTE', "$recep_dte->fEstadoRecepDTE");
                                            $XMLRESPUESTA->writeElement('RecepDTEGlosa', $recep_dte->fRecepDTEGlosa);
                                            $XMLRESPUESTA->endElement();
                                            echo "marca 17</br>";
                                            if ($recep_dte->fEstadoRecepDTE != 3 && $recep_dte->fEstadoRecepDTE != 2 && $recep_dte->fEstadoRecepDTE != 4) {
                                                $dte_xml->save("empresas/$pRutEmpresa/dte_recibidos/$recep_dte->fnombre_archivo");
                                            }
                                        }

                                        $XMLRESPUESTA->endElement(); //FIN DE RECEPCION ENVIO                                   
                                        $XMLRESPUESTA->endElement(); //FIN DE RESULTADO
                                        $XMLRESPUESTA->endElement(); // FIN DE RESPUESTADTE

                                        $XMLRESPUESTA->endDocument();
                                        $XMLRESPUESTA->flush();

                                        if (!$valid) {
                                            unlink($uri);
                                            $transaction->rollback();
                                            throw new Exception('No se logro terminar de guardar el envio');
                                        }
                                        echo "marca 18</br>";
                                        $domRespuesta = new DOMDocument();
                                        $domRespuesta->formatOutput = FALSE;
                                        $domRespuesta->preserveWhiteSpace = TRUE;
                                        $domRespuesta->encoding = "ISO-8859-1";
                                        echo "marca 18.1</br>";
                                        var_dump($uri);
                                        $domRespuesta->load($uri);
                                        $xmlTool = new FR3D\XmlDSig\Adapter\XmlseclibsAdapter();
                                        //debería ser binfactory
                                        //$pfx = file_get_contents("empresas/$pRutEmpresa/certificados/$receptor->certificadoDigital");
                                        $pfx = file_get_contents("fe/base/certificado/certificado.p12");
                                        echo "marca 18.2</br>";
                                        openssl_pkcs12_read($pfx, $key, "kreator3"); //$receptor->fpassword_certificado);
                                        echo "marca 18.3</br>";
                                        $xmlTool->setPrivateKey($key["pkey"]);
                                        $xmlTool->setpublickey($key["cert"]);
                                        echo "marca 18.4</br>";
                                        $xmlTool->addTransform(FR3D\XmlDSig\Adapter\XmlseclibsAdapter::ENVELOPED);
                                        echo "marca 18.5</br>";

                                        var_dump($domRespuesta);

                                        $xmlTool->sign($domRespuesta, "RESPUESTA");
                                        echo "marca 18.6</br>";
                                        $domRespuesta->save($uri);
                                        echo "marca 19</br>";
                                        if ($valid) {
                                            echo "marca 20</br>";
                                            $mail = new PHPMailer;
                                            $mail->isSMTP();
                                            $mail->SMTPDebug = 0;
                                            $mail->Debugoutput = 'html';
                                            $mail->Host = "smtp.gmail.com";
                                            $mail->Port = 587;
                                            $mail->SMTPSecure = 'tls';
                                            $mail->SMTPAuth = true;
                                            $mail->Username = "facturacion@binfactory.cl";
                                            $mail->Password = "kreator45k";
                                            $mail->setFrom('facturacion@binfactory.cl', 'FACTURACION BINFACTORY');
                                            $mail->addAddress($reply_to, $fromaddress);
                                            $mail->Subject = 'RESPUESTA RECEPCION ENVIO DTES';
                                            $mensaje = "RESPUESTA AL ENVIO DE DTES A NUESTRA EMPRESA : binfactory RUT ZZXXX2-2"; // $receptor->razon_social RUT $receptor->rut";
                                            $mail->msgHTML($mensaje);
                                            $mail->addAttachment($uri, "FF_RSPENV_" . $recep_env->frecepcion_envio_ID . ".xml", 'base64', 'application/octet-stream');

                                            //send the message, check for errors
                                            if (!$mail->send()) {
                                                echo "Mailer Error: " . $mail->ErrorInfo;
                                            } else {
                                                echo "RESPUESTAS RECEPCION ENVIADAS\r\n";
                                            }
                                        }
                                    }//FIN ELSE EnvioDtesEsNoValido

                                    break; //FIN SWITCH CASE EnvioDTE
                            }//FIN SWITCH
                        } else {//FIN IF VALID XML
                        }
                    }//FIN IF IS ATTACHMENT
                }//FIN FOREACH ATTACHMENT
            }//FIN FOREACH RESULT
        }//FIN IS ARRAY RESULT
        /*
          $startvalue = 0;
          while ($startvalue < $totalrows) {

          $headers = imap_fetchheader($mbox, $sorted_mbox[ $startvalue ] );
          $subject = array();
          preg_match_all('/^Subject: (.*)/m', $headers, $subject);
          preg_match_all('/^Date: (.*)/m', $headers, $date);
          preg_match_all('/^Date: (.*)/m', $headers, $date);
          preg_match_all('/^From: (.*)/m', $headers, $from);
          print $subject[1][0] . $date[1][0] .  $from[1][0] . "<br/>";
          $startvalue++;
          } */
        //print_r($reader->emails);  
        if ($valid) {
//       $transaction->commit(); 
            echo "ENVIOS RECEPCIONADOS Y ALMACENADOS EN BASE DE DATOS";
            // Yii::app()->user->setFlash('success', "ENVIOS RECEPCIONADOS Y ALMACENADOS EN BASE DE DATOS"); 
        } else {
            // $transaction->rollback(); 
            echo "ERROR AL GUARDAR ENVIOS RECEPCIONADOS";
            //    Yii::app()->user->setFlash('error', "ERROR AL GUARDAR ENVIOS RECEPCIONADOS"); 
        }
    } catch (Exception $e) {
        $transaction->rollback();
        Yii::app()->user->setFlash('error', $e->getMessage());
    }
}

function enviarCorreos() {
    $query = "SELECT * FROM dte_envio_cliente WHERE estado_envio LIKE 'pendiente_envio'";
    $ctrlDTEenvio = new controlador_dte_envio_cliente();
    $ctrlCliente=new controlador_cliente_proveedor();
    $ctrlEmpresaAutorizada=new controlador_empresa_autorizada_dte();
    $arrDteEnvio = $ctrlDTEenvio->buscar($query);
    echo var_dump($arrDteEnvio);
    //'binfactory.dte@gmail.com', 'xqlrubyanaya'
    foreach ($arrDteEnvio as $dteEnvio) {
        $cliente=$ctrlCliente->getCliente_proveedor($dteEnvio->fcliente_proveedor_ID);
        $dteEnvio->frut_cliente=$cliente->frut;
        $query="SELECT * FROM empresa_autorizada_dte WHERE rut LIKE '" . $cliente->frut . "'";
        $arrEmpresa=$ctrlEmpresaAutorizada->buscar($query);
        if($arrEmpresa)
        {
            $empresa=$arrEmpresa[0];
            $dteEnvio->femail_cliente=$empresa->fmail_intercambio;
            $uri = realpath($dteEnvio->fruta_archivo_xml);
            $mail = new PHPMailer;
            $mail->isSMTP();
            $mail->SMTPDebug = 0;
            $mail->Debugoutput = 'html';
            $mail->Host = "smtp.gmail.com";
            $mail->Port = 587;
            $mail->SMTPSecure = 'tls';
            $mail->SMTPAuth = true;
            $mail->Username = "binfactory.dte@gmail.com";
            $mail->Password = "xqlrubyanaya";
            $mail->setFrom('binfactory.dte@gmail.com', 'FACTURACION BINFACTORY');
            $mail->addAddress($dteEnvio->femail_cliente, "binfactory.dte@gmail.com");
            $mail->Subject = 'RESPUESTA RECEPCION ENVIO DTES';
            $mensaje = "RESPUESTA AL ENVIO DE DTES A NUESTRA EMPRESA : binfactory RUT ZZXXX2-2"; // $receptor->razon_social RUT $receptor->rut";
            $mail->msgHTML($mensaje);
            $mail->addAttachment($uri, $dteEnvio->fnombre_archivo, 'base64', 'application/octet-stream');

            //send the message, check for errors
            if (!$mail->send()) {
                echo "Mailer Error: " . $mail->ErrorInfo;
                $dteEnvio->festado_envio="error_envio";
                $dteEnvio->fobservacion="Mailer Error: " . $mail->ErrorInfo;
            } else {
                $dteEnvio->festado_envio="enviado";
                echo "RESPUESTAS RECEPCION ENVIADAS\r\n";
            }
        }
        else {
            
            $dteEnvio->festado_envio="no_facturadora";
            
        }
        $ctrlDTEenvio->actualizar($dteEnvio);
    }
}

function respuestaRecepcionMercaderias($factura_ID,$correo_respuesta)
{
// respuesta en texto plano
header('Content-type: text/plain; charset=ISO-8859-1');

// incluir archivos php de la biblioteca y configuraciones
include 'fe/inc.php';
include_once 'app/apoyo/funciones_auxiliares.php';

// datos
$archivo_recibido = 'xml/intercambio/ENVIO_DTE_521058.xml';
$RutResponde = '76169654-8';;
$RutFirma = '14110944-8';

// Cargar EnvioDTE y extraer arreglo con datos de carátula y DTEs
$EnvioDte = new \sasco\LibreDTE\Sii\EnvioDte();
$EnvioDte->loadXML(file_get_contents($archivo_recibido));
$Caratula = $EnvioDte->getCaratula();
$Documentos = $EnvioDte->getDocumentos();

// caratula
$caratula = [
    'RutResponde' => $RutResponde,
    'RutRecibe' => $Caratula['RutEmisor'],
    //'NmbContacto' => '',
    //'MailContacto' => '',
];

// objeto EnvioRecibo, asignar carátula y Firma
$EnvioRecibos = new \sasco\LibreDTE\Sii\EnvioRecibos();
$EnvioRecibos->setCaratula($caratula);
$EnvioRecibos->setFirma(new \sasco\LibreDTE\FirmaElectronica($config['firma']));

// procesar cada DTE
foreach ($Documentos as $DTE) {
    $EnvioRecibos->agregar([
        'TipoDoc' => $DTE->getTipo(),
        'Folio' => $DTE->getFolio(),
        'FchEmis' => $DTE->getFechaEmision(),
        'RUTEmisor' => $DTE->getEmisor(),
        'RUTRecep' => $DTE->getReceptor(),
        'MntTotal' => $DTE->getMontoTotal(),
        'Recinto' => 'Oficina central',
        'RutFirma' => $RutFirma,
    ]);
}
 file_put_contents("xml" . getDirectorio() . "dte_33_" . $factura_ID . ".xml", $EnvioRecibos->generar());
// generar XML
$xml = $EnvioRecibos->generar();

// validar schema del XML que se generó
if ($EnvioRecibos->schemaValidate()) {
    // mostrar XML al usuario, deberá ser guardado y subido al SII en:
    // https://www4.sii.cl/pfeInternet
    echo $xml;
}

// si hubo errores mostrar
foreach (\sasco\LibreDTE\Log::readAll() as $error)
    echo $error,"\n";    
}
?>