<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once('recepcion_correos/reader.php');
require_once('recepcion_correos/validaciones_recepcion.php');
$prev_url="app";
include_once 'app/clases/cls_cliente_binfactory.php';
include_once "app/controladores/ctrl_cliente_binfactory.php";
include_once "app/fachada/fachada_cliente_binfactory.php";

function leerCorreosDocumentosCompra($cliente_binfactory_ID) {
    $flash = "";
    try {
		escribe_debug("leerCorreosDocumentosCompra.0->" . $cliente_binfactory_ID);  		
        ini_set('max_execution_time', 600);
        $query = "SELECT * FROM cliente_binfactory WHERE ID=" . $cliente_binfactory_ID;
        $ctrlClienteBinfactory = new controlador_cliente_binfactory();
        $arrClienteBinfactory = $ctrlClienteBinfactory->buscar($query);
        $clienteBinfactory=new cliente_binfactory();
        foreach ($arrClienteBinfactory as $clienteBinfactory) {
            
            $cont = 0;
            $correos_a_leer = 20;
			
            $imap_email =$clienteBinfactory->fhost_email;// "imap.gmail.com:993";
            $email = $clienteBinfactory->femail;// "facturacion@binfactory.cl";
            $clave_email =$clienteBinfactory->fclave_email;// "Xxlop90i323";
            escribe_debug("leerCorreosDocumentosCompra.1->" . $clienteBinfactory->fRUTEmisor);
            escribeVarDump($clienteBinfactory);
            $reader = new mail_reader('{' . $imap_email . '/imap/ssl/novalidate-cert/norsh}INBOX', $email, $clave_email);

            $mbox = $reader->connection;
            $valid = true;

            $result = $reader->returnUnseen(); //se retornan los no leidos?

            $cantidad_total_correos_no_leidos = count($result);
            escribe_debug("leerCorreosDocumentosCompra->cantidad_total_correos_no_leidos->" . $cantidad_total_correos_no_leidos);
            //print ("cantidad correos:  $cantidad_total_correos_no_leidos <br>");
            if ($cantidad_total_correos_no_leidos <= 0) {//si no hay correos termina el ciclo
                $valid = false;
            }

            if (is_array($result)) {
                $EnvioDTE = 0;
                $cantidad_correos = 0;
                foreach ($result as $msj) {
                    $cantidad_correos++;

                    if ($cantidad_correos == $correos_a_leer) {
                        echo "salir por $cantidad_correos<br>";
                        break;
                    }
                    echo "procesando";
                    escribe_debug("leerCorreosDocumentosCompra----- $cantidad_correos correos leidos ----- </br>");

                    $structure = imap_fetchstructure($mbox, $msj);
                    $headers = imap_headerinfo($mbox, $msj);
                    escribe_debug("leerCorreosDocumentosCompra->HEADER->" . $headers->Subject);
                    $reply_to = $headers->reply_to[0]->mailbox . "@" . $headers->reply_to[0]->host;
                    $fromaddress = imap_utf8($headers->fromaddress);

                    $attachments = array();
                    if (isset($structure->parts) && count($structure->parts)) {
                        escribe_debug("leerCorreos.1");
                        for ($i = 0; $i < count($structure->parts); $i++) {
                            escribeVarDump($structure->parts[$i]);
                            $attachments[$i] = array(
                                'is_attachment' => false,
                                'filename' => '',
                                'name' => '',
                                'attachment' => ''
                            );

                            if ($structure->parts[$i]->ifdparameters) {
                                escribe_debug("leerCorreos.2");
                                foreach ($structure->parts[$i]->dparameters as $object) {
                                    if (strtolower($object->attribute) == 'filename') {
                                        $attachments[$i]['is_attachment'] = true;
                                        $attachments[$i]['filename'] = $object->value;
                                    }
                                }
                            }

                            if ($structure->parts[$i]->ifparameters) {
                                escribe_debug("leerCorreos.3");
                                foreach ($structure->parts[$i]->parameters as $object) {
                                     escribe_debug("leerCorreos.3.1");
                                    if (strtolower($object->attribute) == 'name') {
                                        escribe_debug("leerCorreos.3.2");
                                        $attachments[$i]['is_attachment'] = true;
                                        escribe_debug("leerCorreos.3.3");
                                        $attachments[$i]['name'] = $object->value;
                                        escribe_debug("leerCorreos.3.4");
                                        escribeVarDump($attachments);
                                    }
                                }
                            }

                            if ($attachments[$i]['is_attachment']) {
                                escribe_debug("leerCorreos.4");
                                $attachments[$i]['attachment'] = imap_fetchbody($mbox, $msj, $i + 1);
                                if ($structure->parts[$i]->encoding == 3) { // 3 = BASE64
                                    $attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
                                } elseif ($structure->parts[$i]->encoding == 4) { // 4 = QUOTED-PRINTABLE
                                    $attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
                                }
                            }
                            else 
                            {
                                escribe_debug("MARCAR_COMO_LEIDO");
                                imap_setflag_full($mbox, $msj, "\\Seen"); //marcar como leido
                            }
                        }
                    }
                    escribe_debug("INICIO_FOR ADJUNTOS");
                    //escribeVarDump($attachments);
                    foreach ($attachments as $adjunto) {

                        //  var_dump($adjunto);
                        if ($adjunto['is_attachment'] == 1) {
                            $tagName="";
                            try {
                                $isXML = true;
                                echo "tiene archivo <br/>";
                                libxml_use_internal_errors(true);
                                $xml = new DOMDocument('1.0');
                                $xml->preserveWhiteSpace = TRUE;

                                if (!$xml->loadXML($adjunto['attachment'])) {
                                    $errors = libxml_get_errors();
                                    $isXML = $isXML && false;
                                    echo "dos <br/>";
                                } else {
                                    $isXML = $isXML && true;
                                    $xml->loadXML($adjunto['attachment']);
                                    $xml->encoding = "ISO-8859-1";
                                    $tagName = $xml->documentElement->tagName;
                                    escribe_debug("leerCorreosDocumentosCompra.tres->" . $tagName);
                                }
                            } catch (Exception $e) {
                                escribe_debug("error1->" . $e->getMessage());
                                echo $e->getMessage();
                                exit();
                            }

                            if ($isXML && $tagName=="EnvioDTE"){// $tagName!="RESULTADO_ENVIO" && $tagName!="RespuestaDTE") {
                                //print("es XML<br>");
                                
                                escribe_debug("leerCorreosDocumentosCompra.CUATRO->" . $tagName);
                                $folder = "dtes_proveedores";
                                if (!is_dir($folder)) {
                                    mkdir($folder, 0777);
                                }
                                $folder="dtes_proveedores/" . $clienteBinfactory->fRUTEmisor;
                                if (!is_dir($folder)) {
                                    mkdir($folder, 0777);
                                }
                                $nombre_archivo="";
                                 if($adjunto['filename']!="")
                                 {
                                     escribe_debug("leerCorreosDocumentosCompra.CINCO");
                                     $nombre_archivo=$adjunto['filename'];
                                 }
                                 else if($adjunto['name']!="")
                                 {
                                     $nombre_archivo=$adjunto['name'];
                                     escribe_debug("leerCorreosDocumentosCompra.SEIS");
                                 }
                                escribe_debug("SE_GUARDA->" . $folder . "/" . $nombre_archivo);
                                $xml->save($folder . "/" . $nombre_archivo); //aca se guarda el XML
                            } else {//FIN IF VALID XML
                            }
                            $cont++;
                        }//FIN IF IS ATTACHMENT
                    }//FIN FOREACH ATTACHMENT
                    imap_setflag_full($mbox, $msj, "\\Seen"); //marcar como leido
                }//FIN FOREACH RESULT
            }//FIN IS ARRAY RESULT
//        if ($valid) {
////       $transaction->commit();
//            echo "ENVIOS RECEPCIONADOS Y ALMACENADOS EN BASE DE DATOS";
//        } else {
//            echo "ERROR AL GUARDAR ENVIOS RECEPCIONADOS";
//        }
        }
        return "todos los correos leidos";
    } catch (Exception $e) {
        // $transaction->rollback();
        escribe_debug("EXCEPCION->" . $e->getMessage());
        return "error";
    }
}
